
const FLAG_SCOPE = 'ae-dices';

const FLAG_TOKEN_ADVANTAGE = 'advantage';
const FLAG_TOKEN_MASTERY = 'mastery';
const FLAG_TOKEN_POWER = 'power';
	
/**
 * Store data and methods related to characer manipulation
 */
export class AECharacterData {

	static getCharacter(actorId) {
	  
	  
	  let actor = null;
	  // FIXME I'm not retrieving the same object if I use this....
	  // (Custom flags are not present)
	  // Don't know why it's not working
	  /*if (actorId) {
		actor = game.actors.tokens[actorId];
		if (!actor) actor = game.actors.get(actorId);
		
	  }*/
	  
	  if( !actor ) {
		  const speaker = ChatMessage.getSpeaker();
		  actor = ChatMessage.getSpeakerActor(speaker);
	  }
	  
	  return actor;
	};
	
	//---------------------------
	// Token manipulations
	//---------------------------
	
	static getAdvantage(actor) {
		let value = actor.getFlag(FLAG_SCOPE, FLAG_TOKEN_ADVANTAGE);
		if( ! value ) { 
			value = 0; 
		}
		return value;
	}
	
	static getMastery(actor) {
		let value = actor.getFlag(FLAG_SCOPE, FLAG_TOKEN_MASTERY);
		if( ! value ) { 
			value = 0; 
		}
		return value;
	}
	
	static getPower(actor) {
		let value = actor.getFlag(FLAG_SCOPE, FLAG_TOKEN_POWER);
		if( ! value ) { 
			value = 0; 
		}
		return value;
	}
	
	/**
	 * Advantage range [-6, 6]
	 */
	static setAdvantage(actor, value) {
		
		if( value < -6 ) {
			value = -6;
		} else if( value > 6 ) {
			value = 6;
		}
		actor.setFlag(FLAG_SCOPE, FLAG_TOKEN_ADVANTAGE, value);
		return value;
	}
	
	/**
	 * Mastery range [-6, 6]
	 */
	static setMastery(actor, value) {
		if( value < -6 ) {
			value = -6;
		} else if( value > 6 ) {
			value = 6;
		}
		actor.setFlag(FLAG_SCOPE, FLAG_TOKEN_MASTERY, value);
		return value;
	}
	
	/**
	 * Power range [0, 3]
	 */
	static setPower(actor, value) {
		if( value < 0 ) {
			value = 0;
		} else if( value > 3 ) {
			value = 3;
		}
		actor.setFlag(FLAG_SCOPE, FLAG_TOKEN_POWER, value);
		return value;
	}
}
