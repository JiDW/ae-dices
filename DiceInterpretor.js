import {AECharacterData} from './CharacterData.js';

const ADVANTAGETOKEN_POSITIVE_ICON = 'modules/ae-dices/textures/tokens/advantage/positive_icon.png';
const ADVANTAGETOKEN_NEGATIVE_ICON = 'modules/ae-dices/textures/tokens/advantage/negative_icon.png';

const MASTERYTOKEN_POSITIVE_ICON = 'modules/ae-dices/textures/tokens/mastery/positive_icon.png';
const MASTERYTOKEN_NEGATIVE_ICON = 'modules/ae-dices/textures/tokens/mastery/negative_icon.png';

const POWERTOKEN_ICON = 'modules/ae-dices/textures/tokens/power/icon.png';

const SUCCESSDICES_ICONS = [
	'modules/ae-dices/textures/success/d6-1_icon.png',
	'modules/ae-dices/textures/success/d6-2_icon.png',
	'modules/ae-dices/textures/success/d6-3_icon.png',
	'modules/ae-dices/textures/success/d6-4_icon.png',
	'modules/ae-dices/textures/success/d6-5_icon.png',
	'modules/ae-dices/textures/success/d6-6_icon.png',
];

const EFFECTDICES_ICONS = [
	'modules/ae-dices/textures/effect/d6-1_icon.png',
	'modules/ae-dices/textures/effect/d6-2_icon.png',
	'modules/ae-dices/textures/effect/d6-3_icon.png',
	'modules/ae-dices/textures/effect/d6-4_icon.png',
	'modules/ae-dices/textures/effect/d6-5_icon.png',
	'modules/ae-dices/textures/effect/d6-6_icon.png',
];

const RISKDICES_ICONS = [
	'modules/ae-dices/textures/risk/d6-1_icon.png',
	'modules/ae-dices/textures/risk/d6-2_icon.png',
	'modules/ae-dices/textures/risk/d6-3_icon.png',
	'modules/ae-dices/textures/risk/d6-4_icon.png',
	'modules/ae-dices/textures/risk/d6-5_icon.png',
	'modules/ae-dices/textures/risk/d6-6_icon.png',
];

export class AEDiceInterpretor {

	static interpretSuccessDice(result, diceFace) {
		
		result.all_icons.push(SUCCESSDICES_ICONS[diceFace-1]);
		result.success_icons.push(SUCCESSDICES_ICONS[diceFace-1]);
		
		if( diceFace == 1 || diceFace == 2 ) {			// 1 - 2 : minus 1
			result.success_value -= 1;

		} else if( diceFace == 3 || diceFace == 4 ) {	// 3 - 4 : plus 1
			result.success_value += 1;
			
		} else if( diceFace == 5 ) {					// 5 : one success token
			result.success_tokens += 1;
			
		} else if( diceFace == 6 ) {					// 6 : two success token
			result.success_tokens += 2;
			
		}
	}

	static interpretEffectDice(result, diceFace) {
		
		result.all_icons.push(EFFECTDICES_ICONS[diceFace-1]);
		
		if( diceFace == 1 ) {							// 1 : minus 1
			result.effect_icons.push(EFFECTDICES_ICONS[diceFace-1]);
			result.effect_value -= 1;

		} else if( diceFace == 2 ) {					// 2 : plus 1
			result.effect_icons.push(EFFECTDICES_ICONS[diceFace-1]);
			result.effect_value += 1;
			
		} else if( diceFace == 3 ) {					// 3 : one effect token
			result.effect_icons.push(EFFECTDICES_ICONS[diceFace-1]);
			result.effect_tokens += 1;
			
		} else if( diceFace == 4 || diceFace == 5 ) {	// 4 - 5 : two success token
			result.effect_icons.push(EFFECTDICES_ICONS[diceFace-1]);
			result.effect_tokens += 2;
			
		} else if( diceFace == 6 ) {					// 6 : one power token
			result.power_tokens += 1;
			result.power_icons.push(EFFECTDICES_ICONS[diceFace-1]);
		}
	}

	static interpretRiskDice(result, diceFace) {
		
		result.all_icons.push(RISKDICES_ICONS[diceFace-1]);
		
		if( diceFace == 1 ) {							// 1 : success + 1
			result.success_icons.push(RISKDICES_ICONS[diceFace-1]);
			result.success_value += 1;

		} else if( diceFace == 2 ) {					// 2 : success + 2
			result.success_icons.push(RISKDICES_ICONS[diceFace-1]);
			result.success_value += 2;
			
		} else if( diceFace == 3 ) {					// 3 : effect - 2
			result.effect_icons.push(RISKDICES_ICONS[diceFace-1]);
			result.effect_value -= 2;
			
		} else if( diceFace == 4 ) {					// 4 : effect token are always skulls
			result.effect_icons.push(RISKDICES_ICONS[diceFace-1]);
			result.always_skulls = true;
		}
														// 5 - 6 : nothing
	}
	
	
	static prepareActorTokens(actor, params) {
		
		// For now, advantage and mastery are precalculated  
		//   meaning advantage -1 is the difference Actor potential and the target defense
		//   same for mastery. 
		// Will change in the future.
		
		let successTokens = AECharacterData.getAdvantage( actor ) + params.advantage;
		successTokens = AECharacterData.setAdvantage( actor, successTokens );
		
		let effectTokens = AECharacterData.getMastery( actor ) + params.mastery;
		effectTokens = AECharacterData.setMastery( actor, effectTokens );
		
		let powerTokens = AECharacterData.getPower( actor );
		// For now, no update on this one during preparation
		
		console.log( 'AE-Dices | Advantage tokens on character before resolving dices : ' + successTokens );
		console.log( 'AE-Dices | Mastery tokens on character before resolving dices : ' + effectTokens );
		console.log( 'AE-Dices | Power tokens on character before resolving dices : ' + powerTokens );
	}
	
	static spendSuccessTokens(result, actor) {
		
		let successTokens = AECharacterData.getAdvantage( actor );
		for( let tokenDisplayOnDice = 0; tokenDisplayOnDice < result.success_tokens; tokenDisplayOnDice++ ) {
			
			if( successTokens > 0 ) {
				result.success_value ++;
				result.success_token_spent.push(ADVANTAGETOKEN_POSITIVE_ICON);
				successTokens --;
				
			} else if( successTokens < 0 ) {
				result.success_value --;
				result.success_token_spent.push(ADVANTAGETOKEN_NEGATIVE_ICON);
				successTokens ++;
				
			}
			// If no tokens left, dice faces with token on them are ignored.
		}
		AECharacterData.setAdvantage( actor, successTokens );
		
		console.log( 'AE-Dices | Advantage tokens on character after resolving dices : ' + successTokens );
	}
	
	static spendEffectTokens(result, actor) {
		
		let effectTokens = AECharacterData.getMastery( actor );
		
		if( result.always_skulls ) {
			result.effect_value -= result.effect_tokens;
			
		} else {
			for( let tokenDisplayOnDice = 0; tokenDisplayOnDice < result.effect_tokens; tokenDisplayOnDice++ ) {
				
				if( effectTokens > 0 ) {
					result.effect_value ++;
					result.effect_token_spent.push(MASTERYTOKEN_POSITIVE_ICON);
					effectTokens --;
					
				} else if( effectTokens < 0 ) {
					result.effect_value --;
					result.effect_token_spent.push(MASTERYTOKEN_NEGATIVE_ICON);
					effectTokens ++;
					
				}
				// If no tokens left, dice faces with token on them are ignored.
			}
			AECharacterData.setMastery( actor, effectTokens );
		}
		
		
		if( result.effect_value <= -3 ) {
			result.eventTriggered = true;
		}
		
		console.log( 'AE-Dices | Mastery tokens on character after resolving dices : ' + effectTokens );
	}
	
	/**
	 * Power tokens represents the level of power the character can unleash.
	 * Maximul level is 3.  (the number of effect dices)
	 * After effect dices are rolled, counts the number of power faces
	 * If it's more than the current level of power of the character, it replaces the precedent value
	 */
	static givePowerTokens(result, actor) {
		
		let powerTokens = AECharacterData.getPower( actor );
		if( result.power_tokens > powerTokens ) {
			AECharacterData.setPower( actor, result.power_tokens );
			console.log( 'AE-Dices | Power tokens on character after resolving dices : ' + result.power_tokens );
		}
	}
	
	/**
	 * Add success label for chat message.
	 * Need the result data to be totally calculated
	 */
	static deduceSuccessLabel(result) {
		
		if( result.success_value <= -5 ) {
			result.success_label = game.i18n.localize("AEDICES.chatResultDisaster");
		} else if( result.success_value <= -3 ) {
			result.success_label = game.i18n.localize("AEDICES.chatResultEpicFail");
		} else if( result.success_value < 0 ) {
			result.success_label = game.i18n.localize("AEDICES.chatResultFailure");
		} else if( result.success_value < 3 ) {
			result.success_label = game.i18n.localize("AEDICES.chatResultSuccess");
		} else if( result.success_value < 5 ) {
			result.success_label = game.i18n.localize("AEDICES.chatResultCritical");
		} else {
			result.success_label = game.i18n.localize("AEDICES.chatResultIncredible");
		}
	}
	
	/**
	 * Add effect label for chat message.
	 * Need the result data to be totally calculated
	 */
	static deduceEffectLabel(result) {
		
		if( result.eventTriggered ) {
			result.effect_label = game.i18n.localize("AEDICES.chatEffectEvent");
		} else {
			result.effect_label = game.i18n.localize("AEDICES.chatEffectNoEvent");
		}
	}
}