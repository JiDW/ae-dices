
import {AECharacterData} from './CharacterData.js';

/**
 * Heavily inspired from FVTT - Token Info Icons
 */
export class AETokenRenderer {

	static async addAdvantageTokens(app, html, data) {
        
        let actor = AECharacterData.getCharacter( data.actorId );
        if( !actor)
            return;
        
		let successTokens = AECharacterData.getAdvantage( actor );

        let newdiv = '<div class="token-info-container">';
        
        let sbutton = '';
		if( successTokens >= 0 ) {
			sbutton = '<div class="control-icon token-info-icon" title="Advantage: ' + successTokens + '"><i class="fas fa-trophy"></i> ' + successTokens + '</div>';
			
		} else {
			successTokens *= -1;
			sbutton = '<div class="control-icon token-info-icon" title="Disadvantage: ' + successTokens + '"><i class="fas fa-times"></i> ' + successTokens + '</div>';
		}
		
        html.find('.attribute.elevation').wrap(newdiv);
        html.find('.attribute.elevation').before(sbutton);
    }

    static async addMasteryTokens(app, html, data) {
        
        let actor = AECharacterData.getCharacter( data.actorId );
        if( !actor)
            return;
        
		let effectTokens = AECharacterData.getMastery( actor );

        let newdiv = '<div class="token-info-container">';
        
        let sbutton = '';
		if( effectTokens >= 0 ) {
			// fa-flower
			sbutton = '<div class="control-icon token-info-icon" title="Safety: ' + effectTokens + '"><i class="fas fa-spa"></i> ' + effectTokens + '</div>';
			
		} else {
			effectTokens *= -1;
			// fa-skull
			sbutton = '<div class="control-icon token-info-icon" title="Danger: ' + effectTokens + '"><i class="fas fa-skull"></i> ' + effectTokens + '</div>';
		}
		
        html.find('.control-icon.config').wrap(newdiv);
        html.find('.control-icon.config').before(sbutton);
    }

    static async addPowerTokens(app, html, data) {
        
        let actor = AECharacterData.getCharacter( data.actorId );
        if( !actor)
            return;
        
		let powerTokens = AECharacterData.getPower( actor );

        let newdiv = '<div class="token-info-container">';
        
        // fa-stars
		let sbutton = '<div class="control-icon token-info-icon" title="Power: ' + powerTokens + '"><i class="fas fa-magic"></i> ' + powerTokens + '</div>';
			
        html.find('.control-icon.target').wrap(newdiv);
        html.find('.control-icon.target').before(sbutton);
    }

}
