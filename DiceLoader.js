import {SuccessDiceDef, EffectDiceDef, RiskDiceDef} from './DiceDefinitions.js';

const AE_DICESYSTEM = {
	id: "AEDices", 
	name: "Acaria Empire All Dices"
};

const AE_DICEPRESETS = {
	'success' : {
		type: "s6",
		labels: [
			"modules/ae-dices/textures/success/d6-1.png",
			"modules/ae-dices/textures/success/d6-2.png",
			"modules/ae-dices/textures/success/d6-3.png",
			"modules/ae-dices/textures/success/d6-4.png",
			"modules/ae-dices/textures/success/d6-5.png",
			"modules/ae-dices/textures/success/d6-6.png"
		],
		system: "AEDices"
	},
	'effect' : {
		type: "e6",
		labels: [
			"modules/ae-dices/textures/effect/d6-1.png",
			"modules/ae-dices/textures/effect/d6-2.png",
			"modules/ae-dices/textures/effect/d6-3.png",
			"modules/ae-dices/textures/effect/d6-4.png",
			"modules/ae-dices/textures/effect/d6-5.png",
			"modules/ae-dices/textures/effect/d6-6.png"
		],
		system: "AEDices"
	},
	'risk' : {
		type: "r6",
		labels: [
			"modules/ae-dices/textures/risk/d6-1.png",
			"modules/ae-dices/textures/risk/d6-2.png",
			"modules/ae-dices/textures/risk/d6-3.png",
			"modules/ae-dices/textures/risk/d6-4.png",
			"modules/ae-dices/textures/risk/d6-5.png",
			"modules/ae-dices/textures/risk/d6-6.png"
		],
		system: "AEDices"
	}
};


export class DiceLoader {

	constructor(dice3d) {

		this.dice3d = dice3d;
		this._declareDices();
		this._loadResources();
	}
	
	_declareDices() {
		CONFIG.Dice.terms["s"] = SuccessDiceDef;
		CONFIG.Dice.terms["e"] = EffectDiceDef;
		CONFIG.Dice.terms["r"] = RiskDiceDef;
	}
	
	_loadResources() {
		
		this.dice3d.addSystem(AE_DICESYSTEM, true);
		
		const modes = ['success', 'effect', 'risk'];
		modes.forEach( (item) => {
			this.dice3d.addDicePreset(AE_DICEPRESETS[item], 'd6');
		});
		
		game.user.setFlag("dice-so-nice", "appearance", {
            colorset: 'white'
		});
	}
	
	retrieveRollDataFromMessage(message) {
		
		let rollData = {
			successIndex : -1,
			successResult : [],
			effectIndex : -1,
			effectResult : [],
			riskIndex : -1,
			riskResult : []
		};
		
		// Build result data
		let diceResults = message._roll.terms;
		for( let i = 0; i < diceResults.length; i++ ) {
			
			if( diceResults[i] instanceof SuccessDiceDef ) {
				
				rollData.successIndex = i;
				
				let results = diceResults[i].results;
				for( let j = 0; j < results.length; j++ ) {
					rollData.successResult.push( results[j].result );
				}
				
				
			} else if( diceResults[i] instanceof EffectDiceDef ) {
				
				rollData.effectIndex = i;
				
				let results = diceResults[i].results;
				for( let j = 0; j < results.length; j++ ) {
					rollData.effectResult.push( results[j].result );
				}
				
				
			} else if( diceResults[i] instanceof RiskDiceDef ) {
				
				rollData.riskIndex = i;
				
				let results = diceResults[i].results;
				for( let j = 0; j < results.length; j++ ) {
					rollData.riskResult.push( results[j].result );
				}
			}
		}
		
		return rollData;
	}

}