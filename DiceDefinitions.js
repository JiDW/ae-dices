
export class SuccessDiceDef extends Die {
	
    constructor(termData) {
        super(termData);
        this.faces = 6;
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = "s";

    /* -------------------------------------------- */

    /** @override */
    static getResultLabel(result) {
        return {
            "1": '<img src="modules/ae-dices/textures/success/d6-1_icon.png" width="24" height="24" style="border:0px">',
            "2": '<img src="modules/ae-dices/textures/success/d6-2_icon.png" width="24" height="24" style="border:0px">',
            "3": '<img src="modules/ae-dices/textures/success/d6-3_icon.png" width="24" height="24" style="border:0px">',
            "4": '<img src="modules/ae-dices/textures/success/d6-4_icon.png" width="24" height="24" style="border:0px">',
            "5": '<img src="modules/ae-dices/textures/success/d6-5_icon.png" width="24" height="24" style="border:0px">',
            "6": '<img src="modules/ae-dices/textures/success/d6-6_icon.png" width="24" height="24" style="border:0px">'
        }[result];
    }
}

export class EffectDiceDef extends Die {
	
    constructor(termData) {
        super(termData);
        this.faces = 6;
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = "e";

    /* -------------------------------------------- */

    /** @override */
    static getResultLabel(result) {
        return {
            "1": '<img src="modules/ae-dices/textures/effect/d6-1_icon.png" width="24" height="24" style="border:0px">',
            "2": '<img src="modules/ae-dices/textures/effect/d6-2_icon.png" width="24" height="24" style="border:0px">',
            "3": '<img src="modules/ae-dices/textures/effect/d6-3_icon.png" width="24" height="24" style="border:0px">',
            "4": '<img src="modules/ae-dices/textures/effect/d6-4_icon.png" width="24" height="24" style="border:0px">',
            "5": '<img src="modules/ae-dices/textures/effect/d6-5_icon.png" width="24" height="24" style="border:0px">',
            "6": '<img src="modules/ae-dices/textures/effect/d6-6_icon.png" width="24" height="24" style="border:0px">'
        }[result];
    }
}

export class RiskDiceDef extends Die {
	
    constructor(termData) {
        super(termData);
        this.faces = 6;
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = "r";

    /* -------------------------------------------- */

    /** @override */
    static getResultLabel(result) {
        return {
            "1": '<img src="modules/ae-dices/textures/risk/d6-1_icon.png" width="24" height="24" style="border:0px">',
            "2": '<img src="modules/ae-dices/textures/risk/d6-2_icon.png" width="24" height="24" style="border:0px">',
            "3": '<img src="modules/ae-dices/textures/risk/d6-3_icon.png" width="24" height="24" style="border:0px">',
            "4": '<img src="modules/ae-dices/textures/risk/d6-4_icon.png" width="24" height="24" style="border:0px">',
            "5": '<img src="modules/ae-dices/textures/risk/d6-5_icon.png" width="24" height="24" style="border:0px">',
            "6": '<img src="modules/ae-dices/textures/risk/d6-6_icon.png" width="24" height="24" style="border:0px">'
        }[result];
    }
}
