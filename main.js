import {DiceLoader} from './DiceLoader.js';
import {AEEventLoader} from './EventLoader.js';
import {AECharacterData} from './CharacterData.js';
import {AEDiceInterpretor} from './DiceInterpretor.js';
import {AEDiceMessage} from './DiceMessage.js';
import {AETokenRenderer} from './TokenRenderer.js';

/**
 * Registers the exposed settings for the various 3D dice options.
 */
Hooks.once('init', () => {

	/*
	game.settings.registerMenu("dice-so-nice", "dice-so-nice", {
        name: "AEDICES.config",
        label: "AEDICES.configTitle",
        hint: "AEDICES.configHint",
        icon: "fas fa-dice-d20",
        type: AEDiceConfig,
        restricted: false
    }); */
});

/**
 * Load structure
 */
Hooks.once('ready', () => {

    game.aedice = new AEDice();
	
	Hooks.on('renderTokenHUD', (app, html, data) => { AETokenRenderer.addAdvantageTokens(app, html, data) });
	Hooks.on('renderTokenHUD', (app, html, data) => { AETokenRenderer.addMasteryTokens(app, html, data) });
	Hooks.on('renderTokenHUD', (app, html, data) => { AETokenRenderer.addPowerTokens(app, html, data) });
});


/**
 * Triggered when dice-so-nice has been loaded
 */
Hooks.once('diceSoNiceReady', (dice3d) => {

	game.aedice.diceLoader = new DiceLoader(dice3d);
});


/**
 * Intercepts messages starting with /aeroll 
 * aeroll syntaxe : /aeroll [advantage] [mastery] <[risk]>'
 * [advantage] : numeric value between -6 and 6
 * [mastery] : numeric value between -6 and 6
 * [risk] : numeric value between 0 and 3. 0 by default.
 */
Hooks.on('createChatMessage', (chatMessage) => {
	
	if( !game.aedice ) {
		return;
	}
	
	let command = AECommands.accepts(chatMessage);
	if( !command ) {
		return;
	}
	
	command.execute.call(game.aedice, command.params);
});

Hooks.on("renderChatMessage", (chatMessage, html, data) => {
	
    if( AECommands._isAERollResult(chatMessage) ) {
		html.find(".dice-roll").hide();
	}
});


export class AECommands {
	
	static accepts(chatMessage) {
	
		if( AECommands._isAERollMessage(chatMessage) ) {
			return AECommands._acceptsAERollMessage(chatMessage.data.content);
			
		} else if( AECommands._isAERollResult(chatMessage) ) {
			return AECommands._acceptsAERollResult(chatMessage);
		}
		
		return null;
	}
	
	static _isAERollMessage(chatMessage) {	
	
		return chatMessage.data 
				&& chatMessage.data.content 
				&& chatMessage.data.content.startsWith( '/aeroll' );
	}
	
	static _isAERollResult(chatMessage) {
		return chatMessage.isRoll 
				&& chatMessage.data.content 
				&& chatMessage.data.content.includes('ds') 
				&& chatMessage.data.content.includes('de');
	}
	
	/**
	 * Alternative to directly call game.aedice.rollDices
	 */
	static _acceptsAERollMessage(data) {
		
		let list = data.split(' ');
		list.shift();
		
		if( list.length < 2 ) {
			console.error('aeroll syntaxe : /aeroll [advantage] [mastery] <[risk]>');
			return null;
		}
		
		if( list.length == 2 ) { list.push(0); }
		
		return {
			execute: game.aedice.rollDices,
			params: {
				advantage: list[0],
				mastery: list[1],
				risk: list[2]
			}
		};
	}
	
	/**
	 * Allow result comupation after rolling ae dices
	 */
	static _acceptsAERollResult(chatMessage) {
		
		return {
			execute: game.aedice.interpretChatRoll,
			params: {
				message: chatMessage
			}
		};
	}
	
}


/**
 * Main class to handle AE 3D Dice animations.
 */
export class AEDice {

	constructor() {

		this._eventLoader = new AEEventLoader();
	}
	
	/**
	 * Allow to update character tokens before rolling dices
	 * params.advantage
	 * params.mastery
	 * params.risk : Optional. If not set, no risk dice will be rolled
	 * params.actorId : <if you want to specify an other actor>
	 */
	rollDices(params) {
		
		// Before rolling dices, update character tokens so that they can be tken into account during roll.
		let actor = this._updateActorTokensWithInteractionDifficulty(params);
		if( ! actor ) {
			return ui.notifications.warn(game.i18n.localize("AEDICES.warnNoActorSpecified"));
		}
		
		let formula = '3ds + 3de';
		if( params.risk > 0 ) {
			formula += ' + ' + params.risk + 'dr';
		}
		let myRoll = new Roll(formula);
		myRoll.roll();
		
		var chatData = {
			type: CONST.CHAT_MESSAGE_TYPES.ROLL,  //it's a roll!
			user: game.user._id,
			speaker: ChatMessage.getSpeaker(),
			roll: myRoll,
			content: formula
		};
			
		ChatMessage.create(chatData, {});
	}
	
	/**
	 * When a roll is done, we need to retrieve the speaker and withdraw tokens.
	 * Without it, we won't knopw if it's a success or not
	 * params.message : The whole chatMessage
	 */
	interpretChatRoll(params) {
		
		let actor = AECharacterData.getCharacter(null);
		if( ! actor ) {
			return ui.notifications.warn(game.i18n.localize("AEDICES.warnNoActorSpecified"));
		}
		
		let rollData = this.diceLoader.retrieveRollDataFromMessage(params.message);
		
		// Compute results
		let result = this._computeResult(rollData, actor);
		
		this._displayResultInChat(result);
	}
	
	/**
	 * params.keepPower
	 * params.actorId : <if you want to specify an other actor>
	 */
	resetTokens(params) {
		
		if (! params) {
			params = {}
		}
		let actor = AECharacterData.getCharacter(params.actorId);
		if( actor ) {
			AECharacterData.setAdvantage(actor, 0);
			AECharacterData.setMastery(actor, 0);
			if( params && ! params.keepPower ) {
				AECharacterData.setPower(actor, 0);
			}
			return ui.notifications.info(game.i18n.localize("AEDICES.infoResetTokenDone"));
		}
	}
		
	
	/**
	 * Update character token before rolling dices.
	 * @returns The updated Actor object. Can return null if there were no Actor linked to the command
	 */
	_updateActorTokensWithInteractionDifficulty(params) {
		
		let actor = AECharacterData.getCharacter(params.actorId);
		if( actor ) {
			AEDiceInterpretor.prepareActorTokens(actor, params);
		}
		return actor;
	}
	
	
	/**
	 * When all dices have been launched, time to compute the result.
	 */
	_computeResult(rollData, actor) {

		let result = {
			all_icons : [],
			success_tokens : 0,
			success_value : 0,
			success_label : '',
			success_icons : [],
			success_token_spent : [],
			effect_tokens : 0,
			effect_value : 0,
			effect_label : '',
			effect_icons : [],
			effect_token_spent : [],
			risk_icons : [],
			power_tokens : 0,
			power_icons : [],
			always_skulls : false,
			eventTriggered: false,
			eventDescription : null
		};
		
		// Store each dice face
		rollData.successResult.forEach( (item) => { AEDiceInterpretor.interpretSuccessDice(result, item); });
		rollData.effectResult.forEach( (item) => { AEDiceInterpretor.interpretEffectDice(result, item); });
		rollData.riskResult.forEach( (item) => { AEDiceInterpretor.interpretRiskDice(result, item); });
		
		// Spend tokens
		AEDiceInterpretor.spendSuccessTokens(result, actor);
		AEDiceInterpretor.spendEffectTokens(result, actor);
		AEDiceInterpretor.givePowerTokens(result, actor);
		
		// Add labels
		AEDiceInterpretor.deduceSuccessLabel(result);
		AEDiceInterpretor.deduceEffectLabel(result);
		
		// Draw event
		if( result.eventTriggered ) {
			let itemData = this._eventLoader.drawCard();
			result.eventDescription = itemData.data.description.value;
		}
		
		return result;
	}
	
	_displayResultInChat(result) {
		
		// Build message
		let messageContent = AEDiceMessage.buildHtmlResult(result);
		
		var chatData = {
			user: game.user._id,
			speaker: ChatMessage.getSpeaker(),
			content: messageContent};

		ChatMessage.create(chatData, {});
	}
}


