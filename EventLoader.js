import {AECharacterData} from './CharacterData.js';

export class AEEventLoader {

	/**
	 * Shoud have aleady been loaded in module.
	 * Convenience method to reload it from scratch if card changes
	 */
	loadCompendium() {
		
		// Reference a Compendium pack by it's collection ID
		let pack = game.packs.find(p => p.collection === 'ae-dices.ae-eventcards');
        if( ! pack ) {
        	return ui.notifications.warn(game.i18n.localize("AEDICES.packEventCardAbsent"));
			
        } else if( pack.locked ) {
        	return ui.notifications.warn(game.i18n.localize("AEDICES.packEventCardLocked"));
        }

		for ( let index = 0;  index < 108; index++ ) {
		
			let itemData = this._buildEventCardData(index);
			
			pack.importEntity(new Item(itemData));
			console.log( 'Imported item' + suffix + ' into Compendium pack world.AE-EventCards');
		}
	}
	
	drawCard() {
		
		let pack = game.packs.find(p => p.collection === 'ae-dices.ae-eventcards');
        if( ! pack ) {
        	return ui.notifications.warn(game.i18n.localize("AEDICES.packEventCardAbsent"));
        }
		
		let actor = AECharacterData.getCharacter(null);
		if( ! actor ) {
			return ui.notifications.warn(game.i18n.localize("AEDICES.warnNoActorSpecified"));
		}
		
		let itemData = this._buildEventCardData(Math.floor(Math.random() * 108));
		let test = actor.createEmbeddedEntity("OwnedItem", itemData); 
		return itemData;
	}
	
	
	_buildEventCardData(index) {
		
		let indexS = '' + index;
		let suffix = '000'.substring(0, 3 - indexS.length) + indexS;
	
		let imagePath = 'modules/ae-dices/eventcards/carte_' + index + '.png';
	
		let itemData = {
			type: 'loot', 
			name: game.i18n.localize("AEDICES.cardDrawnEventItemName") + ' ' + suffix, 
			img: 'modules/ae-dices/eventcards/event_icon.png',
			data: {
				description: {
					value: '<img src="' + imagePath + '" title="' + suffix + '">'
				}
			}
		};
		
		return itemData;
	}
}