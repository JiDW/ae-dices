
const ROLLMESSAGE_BASE = '' +
'	<div class="dice-roll">' +
'		<div class="dice-result">' +
'			<div class="dice-formula">%TITLE%</div>' +
'			<div class="dice-tooltip" style="display: block;">' +

'		<section class="tooltip-part">' +
'			<div class="dice">' +
'				<header class="part-header flexrow">' +
'					<span class="part-formula">%ALL_DICES_TITLE%</span>' +
'				</header>' +
'				<ol class="dice-rolls">' +
'					%ALL_DICES_DETAILS%' +
'				</ol>' +
'			</div>' +
'		</section>' +


'		<section class="tooltip-part">' +
'			<div class="dice">' +
'				<header class="part-header flexrow">' +
'					<span class="part-formula">%SUCCESS_DICES_TITLE%</span>' +
'					<span class="part-total">%SUCCESS_DICES_RESULT%</span>' +
'				</header>' +
'				<ol class="dice-rolls">' +
'					%SUCCESS_DICES_DETAILS%' +
'				</ol>' +
'			</div>' +
'		</section>' +


'		<section class="tooltip-part">' +
'			<div class="dice">' +
'				<header class="part-header flexrow">' +
'					<span class="part-formula">%EFFECT_DICES_TITLE%</span>' +
'					<span class="part-total">%EFFECT_DICES_RESULT%</span>' +
'				</header>' +
'				<ol class="dice-rolls">' +
'					%EFFECT_DICES_DETAILS%' +
'				</ol>' +
'			</div>' +
'		</section>' +

'		<section class="tooltip-part">' +
'			<div class="dice">' +
'				<header class="part-header flexrow">' +
'					<span class="part-formula">%POWER_DICES_TITLE%</span>' +
'					<span class="part-total">%POWER_DICES_RESULT%</span>' +
'				</header>' +
'				<ol class="dice-rolls">' +
'					%POWER_DICES_DETAILS%' +
'				</ol>' +
'			</div>' +
'		</section>' +

'	</div>' +

'			<div class="dice-formula">%INTERACTION_RESULT%</div>' +
'			<div class="dice-formula">%EVENT_RESULT%</div>' +
'           <p class="part-formula">%EVENT_DESCRIPTION%</p>'
'		</div>' +
'	</div>';

const ROLLICON_BASE = '<li class="roll d6"><img src="%DICE_ICON%" width="24" height="24" style="border:0px"></li>';


export class AEDiceMessage {
	

	static _buildDiceDetails(iconArray) {
		
		let details = '';
		for( let index = 0; index < iconArray.length; index++ ) {
			details += ROLLICON_BASE.replace('%DICE_ICON%', iconArray[index]);
		}
		return details;
	}
	
	static buildHtmlResult(result) {
		
		let html = ROLLMESSAGE_BASE;
		
		html = html.replace('%TITLE%', game.i18n.localize("AEDICES.chatCustomInteraction") );
		
		html = html.replace('%ALL_DICES_TITLE%', game.i18n.localize("AEDICES.chatRolledDices") );
		html = html.replace('%ALL_DICES_DETAILS%', AEDiceMessage._buildDiceDetails(result.all_icons) );
		
		html = html.replace('%SUCCESS_DICES_TITLE%', game.i18n.localize("AEDICES.chatSucessLevel") );
		html = html.replace('%SUCCESS_DICES_RESULT%', '' + result.success_value );
		html = html.replace('%SUCCESS_DICES_DETAILS%', AEDiceMessage._buildDiceDetails(result.success_icons) );
		
		html = html.replace('%EFFECT_DICES_TITLE%', game.i18n.localize("AEDICES.chatEffectlevel") );
		html = html.replace('%EFFECT_DICES_RESULT%', '' + result.effect_value );
		html = html.replace('%EFFECT_DICES_DETAILS%', AEDiceMessage._buildDiceDetails(result.effect_icons) );
		
		html = html.replace('%POWER_DICES_TITLE%', game.i18n.localize("AEDICES.chatPowerlevel") );
		html = html.replace('%POWER_DICES_RESULT%', '' + result.power_tokens );
		html = html.replace('%POWER_DICES_DETAILS%', AEDiceMessage._buildDiceDetails(result.power_icons) );
		
		html = html.replace('%INTERACTION_RESULT%', result.success_label );
		html = html.replace('%EVENT_RESULT%', result.effect_label );
		
		if( result.eventTriggered ) {
			html = html.replace('%EVENT_DESCRIPTION%', result.eventDescription );
			
		} else {
			html = html.replace('%EVENT_DESCRIPTION%', '' );
		}
		return html;
	}
	
}
